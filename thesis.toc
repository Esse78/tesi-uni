\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{Elenco delle figure}{ix}{chapter*.3}%
\contentsline {chapter}{\numberline {1}Introduzione}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Contesto generale in cui si colloca questo lavoro: la sicurezza dei sistemi embedded}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Breve storia della sicurezza informatica}{1}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Perché questo è importante}{2}{subsection.1.2.1}%
\contentsline {section}{\numberline {1.3}Problema specifico affrontato in questa tesi}{3}{section.1.3}%
\contentsline {section}{\numberline {1.4}Struttura della tesi}{3}{section.1.4}%
\contentsline {chapter}{\numberline {2}Sistemi Embedded}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Cos'è un sistema embedded}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Storia dei sistemi embedded}{5}{subsection.2.1.1}%
\contentsline {section}{\numberline {2.2}Yocto}{6}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Termini importanti per capire yocto}{6}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}La struttura di yocto}{7}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Perché usare yocto}{9}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Svantaggi dell'uso di yocto}{11}{subsection.2.2.4}%
\contentsline {section}{\numberline {2.3}ESF}{11}{section.2.3}%
\contentsline {chapter}{\numberline {3}Sicurezza Informatica}{15}{chapter.3}%
\contentsline {section}{\numberline {3.1}La triade CIA}{15}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Cos'è la riservatezza?}{15}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Cos'è l'integrità?}{16}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Cos'è la disponibilità?}{16}{subsection.3.1.3}%
\contentsline {section}{\numberline {3.2}I due approcci della sicurezza}{17}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Intrusion Prevention System (IPS)}{17}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Intrusion Detection System (IDS)}{19}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}IPS vs IDS}{20}{subsection.3.2.3}%
\contentsline {section}{\numberline {3.3}SCAP}{20}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}SCAP content}{22}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}SCAP scanner}{22}{subsection.3.3.2}%
\contentsline {chapter}{\numberline {4}Analisi del progetto e progetto della soluzione}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}Sistema embedded usato}{23}{section.4.1}%
\contentsline {section}{\numberline {4.2}Analisi problema}{25}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Requisiti funzionali}{25}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Requisiti non-funzionali}{25}{subsection.4.2.2}%
\contentsline {chapter}{\numberline {5}Implementazioni}{27}{chapter.5}%
\contentsline {section}{\numberline {5.1}Fail2Ban}{28}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Analisi/benefici Fail2Ban}{28}{subsection.5.1.1}%
\contentsline {section}{\numberline {5.2}OpenSCAP}{30}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Analisi/benefici OpenSCAP}{31}{subsection.5.2.1}%
\contentsline {section}{\numberline {5.3}AIDE}{32}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}Analisi/benefici AIDE}{32}{subsection.5.3.1}%
\contentsline {section}{\numberline {5.4}Samhain}{33}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Analisi/benefici Samhain}{33}{subsection.5.4.1}%
\contentsline {section}{\numberline {5.5}Snort}{34}{section.5.5}%
\contentsline {subsection}{\numberline {5.5.1}Analisi/benefici Snort}{36}{subsection.5.5.1}%
\contentsline {section}{\numberline {5.6}Suricata}{37}{section.5.6}%
\contentsline {subsection}{\numberline {5.6.1}Analisi/benefici Suricata}{40}{subsection.5.6.1}%
\contentsline {section}{\numberline {5.7}SELinux}{41}{section.5.7}%
\contentsline {subsection}{\numberline {5.7.1}Come funziona SELinux}{41}{subsection.5.7.1}%
\contentsline {subsection}{\numberline {5.7.2}SELinux policy}{43}{subsection.5.7.2}%
\contentsline {subsection}{\numberline {5.7.3}Analisi/benefici Selinux}{44}{subsection.5.7.3}%
\contentsline {section}{\numberline {5.8}Tabella riassuntiva}{45}{section.5.8}%
\contentsline {subsection}{\numberline {5.8.1}Legenda Tabella}{45}{subsection.5.8.1}%
\contentsline {section}{\numberline {5.9}Implementazione}{45}{section.5.9}%
\contentsline {chapter}{\numberline {6}Valutazione conclusiva progetto}{47}{chapter.6}%
\contentsline {section}{\numberline {6.1}Cosa è stato fatto}{47}{section.6.1}%
\contentsline {section}{\numberline {6.2}Valutazione}{47}{section.6.2}%
\contentsline {section}{\numberline {6.3}Possibili sviluppi futuri}{48}{section.6.3}%
\contentsline {chapter}{Bibliografia}{51}{chapter*.25}%
